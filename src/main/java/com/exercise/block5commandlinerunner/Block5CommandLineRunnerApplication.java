package com.exercise.block5commandlinerunner;

import jakarta.annotation.PostConstruct;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class Block5CommandLineRunnerApplication implements CommandLineRunner {


	public static void main(String[] args) {
		SpringApplication.run(Block5CommandLineRunnerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
	@Bean
	CommandLineRunner funcion2(){
		return p->{
			System.out.println("Hola desde clase secundaria");
		};
	}
	@Bean
	CommandLineRunner funcion3(){

		return args->{
			for (String a:args){
				System.out.println(a);
			}
			//System.out.println(args.toString());
		};
	}

	@PostConstruct
	private void funcion1(){
		System.out.println("Hola desde clase inicial");
	}

	/**
	 * Se ejecuta antes la funcion1 pese a estar en ultimo lugar. El PostConstruct se ejecuta justo despues
	 * de la inyeccion de dependencias por lo que se va a ejecutar antes que las otras funciones
	 */

}
